'use strict';
const { MessageModel } = require('@oracle/bots-node-sdk/lib');
// const fs  = require('fs');
// const {run} = require('../bot2');

module.exports = {
  metadata: () => ({
    name: 'call.puppeteer',
    properties: {
      human: { required: true, type: 'string' },
    },
    supportedActions: ['weekday', 'weekend']
  }),
  invoke: (conversation, done) => {
    // perform conversation tasks.
    const { human } = conversation.properties();
    // determine date
    const now = new Date();
    const dayOfWeek = now.toLocaleDateString('en-US', { weekday: 'long' });
    const isWeekend = [0, 6].indexOf(now.getDay()) > -1;
    
    let rawdata = fs.readFileSync('sample.json');
    let instructions = JSON.parse(rawdata);

    run(instructions);
    
    // reply
    const dataurl = "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSA3ewPXCeBX905QLv4U7qiu7xteiepbLC6C7KW4e6Ni-6FN8dW&usqp=CAU"
    conversation.reply(MessageModel.attachmentConversationMessage("image", dataurl, null, null))

    conversation
      .reply(`Greetings ${human}`)
      .reply(`Today is ${now.toLocaleDateString()}, a ${dayOfWeek}`)
      .keepTurn(true)
      .transition(isWeekend ? 'weekend' : 'weekday');
 
    done();
  }
};
