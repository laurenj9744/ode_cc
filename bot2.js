const puppeteer = require("puppeteer");

const actionsMapper = {
    newPage: async browser => {
      await browser.newPage();
      console.log("Opened a new page...");
    },
    click: async (page, { selector }) => {
      await page.waitForSelector(selector);
      await page.click(selector);
      console.log(`Clicked on ${selector}...`);
    },
    goto: async (page, { url }) => {
      await page.goto(url);
      console.log(`Went to ${url}...`);
    },
    type: async (page, { selector, value }) => {
      await page.waitForSelector(selector);
      await page.type(selector, value);
      console.log(`Typed on ${selector}, value: ${value}...`);
    },
    setViewport: async (page, { width, height }) => {
      await page.setViewport({ width, height });
      console.log(`Setting viewport of browser to h:${height} and w:${width}`);
    }
  };
  
  exports.run = async (instructions) => {
    // ToDo see if we can sandbox the execution
    const browser = await puppeteer.launch({
      headless: false,
      slowMo: 200,
      args: [
        "--no-sandbox",
        "--single-process",
        "--disable-setuid-sandbox",
        "--disable-gpu",
        "--disable-extensions",
        "--disable-dev-shm-usage"
      ]
    });
    console.log("Launched puppeteer...");
    // ToDo add newPage as a command?
    const page = await browser.newPage();
    console.log("Opened new page...");
    for (const { command, options } of instructions) {
      console.log(`Executing command ${command}`);
      await actionsMapper[command](page, options);
    }
    console.log("Finished script!");
    console.log("Closing browser...");
    browser.close();
    return { message: "I've done what you asked Sir." };
  };
  